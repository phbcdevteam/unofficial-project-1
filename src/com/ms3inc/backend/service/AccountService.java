package com.ms3inc.backend.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ms3inc.backend.dao.GenericDao;
import com.ms3inc.backend.daoimplementation.AddressDaoImplementation;
import com.ms3inc.backend.daoimplementation.ContactDaoImplementation;
import com.ms3inc.backend.daoimplementation.NameDaoImplementation;
import com.ms3inc.backend.daoimplementation.UserDaoImplementation;
import com.ms3inc.backend.domain.Account;
import com.ms3inc.backend.domain.Address;
import com.ms3inc.backend.domain.Contact;
import com.ms3inc.backend.domain.Name;
import com.ms3inc.backend.domain.User;

@Service
public class AccountService {

	@Autowired
	private UserDaoImplementation userDao;
	
	@Autowired
	private NameDaoImplementation nameDao;
	
	@Autowired
	private AddressDaoImplementation addressDao;
	
	@Autowired
	private ContactDaoImplementation contactDao;
	
	
	public void createAccount(Account register) {
		
		User user = new User(register.getUsername(),register.getPassword());
		Name name = new Name(register.getFirstName(),register.getMiddleName(), register.getLastName());
		Address address = new Address(register.getZipCode(),register.getStreetName(),register.getTownName(),register.getProvinceName());
		Contact contact = new Contact(register.getPhoneNumber(),register.getEmailAddress());
		
		((GenericDao) userDao).insert(user);
		((GenericDao) nameDao).insert(name);
		((GenericDao) addressDao).insert(address);
		((GenericDao) contactDao).insert(contact);
		
	}
	
	public Account getProfile(String username) {
		return userDao.getAccountByUsername(username);
	}
	
	public int getProfileID(String username) {
		return userDao.getIdByUsername(username);
	}
	
	public boolean accountExist(String username, String password) {
		return userDao.accountExist(username, password);
	}
	
	public boolean usernameExist(String username) {
		return userDao.userExist(username);
	}
	
	public boolean emailAddressExist(String emailAddress) {
		return userDao.emailExist(emailAddress);
	}
	
	public boolean phoneNumberExist(String phoneNumber) {
		return userDao.phoneExist(phoneNumber);
	}
	
}
