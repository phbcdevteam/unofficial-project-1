package com.ms3inc.backend.service;

import java.util.Map;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ms3inc.backend.dao.GenericDao;
import com.ms3inc.backend.daoimplementation.PollDaoImplementation;
import com.ms3inc.backend.daoimplementation.ResponseDaoImplementation;
import com.ms3inc.backend.domain.Poll;

@Service
public class PollService {

	@Autowired
	private PollDaoImplementation pollDao;
	
	@Autowired
	private ResponseDaoImplementation responseDao;
	
	public Map<String, Object> createPoll(int id, Poll poll) {
		String generatedCode = generateRandom(32);
		Poll newPoll = new Poll(id,generatedCode,poll.getTitle());
		((GenericDao) pollDao).insert(newPoll);
		return responseDao.genericResponse(true, generatedCode);
	}
	
	public Map<String, Object> updatePoll(int id, int userid, Poll poll){
		Poll newPoll = new Poll(id,userid,poll.getTitle());
		if (((GenericDao) pollDao).update(newPoll)) {
			return responseDao.genericResponse(true, "Poll title was changed to '" + poll.getTitle() + "'.");
		}else {
			return responseDao.genericResponse(false, "No changes happened. Make sure that you are the owner of this poll or the poll should be existed.");
		}
	}
	
	public Map<String, Object> deletePoll(int id, int userid){
		Poll newPoll = new Poll(id,userid);
		if (((GenericDao) pollDao).delete(newPoll)) {
			return responseDao.genericResponse(true, "Poll has been deleted.");
		}else {
			return responseDao.genericResponse(false, "No changes happened. Make sure that you are the owner of this poll or the poll should be existed.");
		}
	}
	
	public Map<String, Object> getPollList(int userid){
		if (!pollDao.getAllPollsByUserId(userid).isEmpty()) {
			return responseDao.listResponse(true, pollDao.getAllPollsByUserId(userid));
		}else {
			return responseDao.genericResponse(false, "No available poll.");
		}
	}
	
	private String generateRandom(int length) {
		StringBuilder sb = new StringBuilder(length);
		Random random = new Random();
		String Sets = "AaBbCcDdEeFfGg0HhIi9JjKk8LlMm7NnOo6PpQq5RrSs4TtUu3VvWw2XxYy1Zz";
		for (int i = 0; i < length; i++) {
			sb.append(Sets.charAt(random.nextInt(Sets.length())));
		}
		return sb.toString();
	}
	
}
