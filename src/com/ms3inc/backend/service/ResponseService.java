package com.ms3inc.backend.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ms3inc.backend.daoimplementation.ResponseDaoImplementation;

@Service
public class ResponseService {

	@Autowired
	private ResponseDaoImplementation responseDao;
	
	public Map<String, Object> genericResponse(boolean status, String messages){
		return responseDao.genericResponse(status, messages);
	}
	
	public Map<String, Object> tokenResponse(boolean status, String token){
		return responseDao.tokenResponse(status, token);
	}
	
	public Map<String, Object> listResponse(boolean status, List<Object> list){
		return responseDao.listResponse(status, list);
	}
	
}
