package com.ms3inc.backend.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import com.ms3inc.backend.service.ResponseService;

@RestController
@SessionAttributes("token")
public class DashboardController {

	@Autowired
	private ResponseService responseService;
	
	@GetMapping("/dashboard")
	public Map<String, Object> processDashboard(HttpSession session, HttpServletRequest request) {
	
		if (session.getAttribute("token") != null) {
			// Show uuid of the user.
			return responseService.tokenResponse(true, (String) session.getAttribute("token"));
		}
		
		return responseService.tokenResponse(false, "");
	}
	
	@DeleteMapping("/logout/{token}")
	public Map<String, Object> processLogout(@PathVariable String token, HttpSession session, SessionStatus status, HttpServletRequest request) {
		
		if(session.getAttribute("token") != null) {
			if(((String) session.getAttribute("token")).equals(token)) {
				
				// Remove attribute token making it null.
				session.removeAttribute("token");
				
				// Completing sessions and invalidating it.
				status.setComplete();
				session.invalidate();
				
				return responseService.genericResponse(true, "Success.");
			}
		}
		return responseService.genericResponse(false, "Failed. Token may already expired or not exist.");
	}
	
}
