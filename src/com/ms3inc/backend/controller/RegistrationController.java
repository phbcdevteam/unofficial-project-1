package com.ms3inc.backend.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.ms3inc.backend.domain.Account;
import com.ms3inc.backend.service.AccountService;
import com.ms3inc.backend.service.ResponseService;

@RestController
public class RegistrationController {

	@Autowired
	private AccountService accountService;
	
	@Autowired
	private ResponseService responseService;
	
	@PostMapping(path="/register",consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public Map<String,Object> processRegistration(@RequestBody Account account){
		
		if (accountService.usernameExist(account.getUsername())) {
			return responseService.genericResponse(false, "Username was already exist. Please pick another one.");
		}
		
		if (accountService.emailAddressExist(account.getEmailAddress())) {
			return responseService.genericResponse(false, "Email Address was already exist. Please use another one.");
		}
		
		if(accountService.phoneNumberExist(account.getPhoneNumber())) {
			return responseService.genericResponse(false, "Phone number was already exist. Please use another one.");
		}
		
		accountService.createAccount(account);
		
		return responseService.genericResponse(true, "Success. You may now login with your username and password.");
		
	}
	
}
