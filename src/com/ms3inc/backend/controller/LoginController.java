package com.ms3inc.backend.controller;

import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.ms3inc.backend.domain.Account;
import com.ms3inc.backend.service.AccountService;
import com.ms3inc.backend.service.ResponseService;

@RestController
@SessionAttributes({"token","profile"})
public class LoginController {

	@Autowired
	private AccountService accountService;
	
	@Autowired
	private ResponseService responseService;
	
	private UUID uuid;
	
	
	@PostMapping(path="/login",consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public Map<String, Object> processLogin(@RequestBody Account account, 
								HttpSession session){
		
		// Creating universal unique identifier.
		uuid = UUID.randomUUID();
		
		if(!accountService.usernameExist(account.getUsername())) {
			// No username exist.
			return responseService.genericResponse(false, "No such username exist. Please signup first before signing in again.");
		}
		
		if(accountService.accountExist(account.getUsername(), account.getPassword())) {
			
			// Setting attribute 'token' with the created uuid.
			session.setAttribute("token", uuid.toString());
			session.setAttribute("profile", accountService.getProfile(account.getUsername()));
			
			return responseService.genericResponse(true, "" + accountService.getProfileID(account.getUsername()));
		}
		
		// Failed password.
		return responseService.genericResponse(false, "You have entered a wrong password.");
		
	}
	
}
