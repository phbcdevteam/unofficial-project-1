package com.ms3inc.backend.controller;

import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.ms3inc.backend.domain.Account;
import com.ms3inc.backend.domain.Poll;
import com.ms3inc.backend.service.PollService;
import com.ms3inc.backend.service.ResponseService;

@RestController
@RequestMapping("/poll")
@SessionAttributes({"token","profile"})
public class PollController {
	
	@Autowired
	private PollService pollService;
	
	@Autowired
	private ResponseService responseService;
	
	@PostMapping(path="/create",consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public Map<String, Object> processCreatePoll(@RequestBody Poll poll, HttpSession session){
		if(session.getAttribute("profile") != null) {
			Account account = (Account) session.getAttribute("profile");
			return pollService.createPoll(account.getId(), poll);
		}else {
			return responseService.genericResponse(false, "You are currently not logged in.");
		}
	}
	
	@PostMapping(path="/update/{id}",consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public Map<String, Object> processUpdatePoll(@RequestBody Poll poll, @PathVariable int id, HttpSession session){
		if(session.getAttribute("profile") != null) {
			Account account = (Account) session.getAttribute("profile");
			return pollService.updatePoll(id, account.getId(), poll);
		}else {
			return responseService.genericResponse(false, "You are currently not logged in.");
		}
	}
	
	@GetMapping(path="/delete/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public Map<String, Object> processDeletePoll(@PathVariable int id, HttpSession session){
		if(session.getAttribute("profile") != null) {
			Account account = (Account) session.getAttribute("profile");
			return pollService.deletePoll(id, account.getId());
		}else {
			return responseService.genericResponse(false, "You are currently not logged in.");
		}
	}
	
	@GetMapping(path="/list", produces = MediaType.APPLICATION_JSON_VALUE)
	public Map<String, Object> processGetPoll(HttpSession session){
		if(session.getAttribute("profile") != null) {
			Account account = (Account) session.getAttribute("profile");
			return pollService.getPollList(account.getId());
		}else {
			return responseService.genericResponse(false, "You are currently not logged in.");
		}
	}
	
}
