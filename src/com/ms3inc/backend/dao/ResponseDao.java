package com.ms3inc.backend.dao;

import java.util.List;
import java.util.Map;

public interface ResponseDao {

	public Map<String, Object> genericResponse(boolean status,String messages);
	public Map<String, Object> tokenResponse(boolean status, String token);
	public <T> Map<String, Object> listResponse(boolean status, List<T> list);
}
