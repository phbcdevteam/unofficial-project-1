package com.ms3inc.backend.dao;

import java.util.List;

import com.ms3inc.backend.domain.Poll;


public interface PollDao {
	
	public List<Poll> getAllPollsByUserId(int userid);
	
}
