package com.ms3inc.backend.dao;

import com.ms3inc.backend.domain.Account;
import com.ms3inc.backend.domain.User;

public interface AccountDao {

	public Account getAccountByUsername(String username);
	public User getUserById(String username);
	
	public int getIdByUsername(String username);
	
	public boolean isUser(int id);
	public boolean userExist(String username);
	public boolean emailExist(String emailAddress);
	public boolean phoneExist(String phoneNumber);
	public boolean accountExist(String username, String password);
	
}
