package com.ms3inc.backend.dao;

import javax.sql.DataSource;

public interface GenericDao {

	public void setDataSource(DataSource ds);
	public boolean insert(Object obj);
	public boolean update(Object obj);
	public boolean delete(Object obj);
	
}
