package com.ms3inc.backend.daoimplementation;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Component;

import com.ms3inc.backend.dao.GenericDao;
import com.ms3inc.backend.dao.PollDao;
import com.ms3inc.backend.domain.Poll;

@Component
public class PollDaoImplementation implements GenericDao, PollDao {
	
	private NamedParameterJdbcTemplate namedParamJdbcTemplate;

	@Autowired
	@Override
	public void setDataSource(DataSource ds) {
		namedParamJdbcTemplate = new NamedParameterJdbcTemplate(ds);
		
	}
	
	@Override
	public boolean insert(Object obj) {
		Poll poll = (Poll) obj;
		SqlParameterSource source = new BeanPropertySqlParameterSource(poll);
		
		String sql = "INSERT INTO polls(UserID, PollCode, PollTitle) VALUES(:userid, :code, :title)";
		
		if (namedParamJdbcTemplate.update(sql, source) > 0) {
			return true;
		}
		
		return false;
	}

	@Override
	public boolean update(Object obj) {
		Poll poll = (Poll) obj;
		SqlParameterSource beanParams = new BeanPropertySqlParameterSource(poll);
		String sqlQuery = "UPDATE polls SET PollTitle = :title WHERE ID = :id AND UserID = :userid";
		return namedParamJdbcTemplate.update(sqlQuery, beanParams) == 1;
	}

	@Override
	public boolean delete(Object obj) {
		Poll poll = (Poll) obj;
		SqlParameterSource beanParams = new BeanPropertySqlParameterSource(poll);
		String sqlQuery = "DELETE FROM polls WHERE ID = :id AND UserID = :userid";
		return namedParamJdbcTemplate.update(sqlQuery, beanParams) == 1;
	}
	
	@Override
	public List<Poll> getAllPollsByUserId(int userid) {
		SqlParameterSource params = new MapSqlParameterSource("UserID", userid);
		String sql = "SELECT * FROM polls WHERE UserId = :UserID";
		
		List<Poll> pollList = namedParamJdbcTemplate.query(sql, params, new PollRowMapper());
		return pollList;
	}

	class PollRowMapper implements RowMapper<Poll>{

		@Override
		public Poll mapRow(ResultSet rs, int rowNum) throws SQLException {
			Poll poll = new Poll();
			poll.setId(rs.getInt("ID"));
			poll.setUserid(rs.getInt("UserID"));
			poll.setCode(rs.getString("PollCode"));
			poll.setTitle(rs.getString("PollTitle"));
			return poll;
		}
		
	}

	

	
	
}
