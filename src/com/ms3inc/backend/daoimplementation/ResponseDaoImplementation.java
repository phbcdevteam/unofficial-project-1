package com.ms3inc.backend.daoimplementation;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.ms3inc.backend.dao.ResponseDao;

@Component
public class ResponseDaoImplementation implements ResponseDao {

	@Override
	public Map<String, Object> genericResponse(boolean status, String messages) {
		Map<String, Object> response = new LinkedHashMap<String,Object>();
		response.put("status", status);
		response.put("message", messages);
		return response;
	}

	@Override
	public Map<String, Object> tokenResponse(boolean status, String token) {
		Map<String, Object> response = new LinkedHashMap<String,Object>();
		response.put("status", status);
		response.put("token", token);
		return response;
	}

	@Override
	public <T> Map<String, Object> listResponse(boolean status, List<T> list) {
		Map<String, Object> response = new LinkedHashMap<String,Object>();
		response.put("status", status);
		response.put("result", list);
		return response;
	}

	

	
}
