package com.ms3inc.backend.daoimplementation;

import java.sql.ResultSet;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Component;

import com.ms3inc.backend.dao.AccountDao;
import com.ms3inc.backend.dao.GenericDao;
import com.ms3inc.backend.domain.Account;
import com.ms3inc.backend.domain.User;

@Component
public class UserDaoImplementation implements GenericDao,AccountDao {

	private NamedParameterJdbcTemplate namedParamJdbcTemplate;
	
	@Autowired
	@Override
	public void setDataSource(DataSource ds) {
		namedParamJdbcTemplate = new NamedParameterJdbcTemplate(ds);
	}

	@Override
	public boolean insert(Object obj) {
		User user = (User) obj;
		SqlParameterSource source = new BeanPropertySqlParameterSource(user);
		
		String sql = "INSERT INTO users(Username, Password) VALUES(:username, MD5(:password))";
		
		if (namedParamJdbcTemplate.update(sql, source) > 0) {
			return true;
		}
		
		return false;
	}

	@Override
	public boolean update(Object obj) {
		// TODO Auto-generated method stub
		return false;
	}
	
	@Override
	public boolean delete(Object obj) {
		// TODO Auto-generated method stub
		return false;
	}


	@Override
	public User getUserById(String username) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean isUser(int id) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public int getIdByUsername(String username) {
		MapSqlParameterSource source = new MapSqlParameterSource();
		source.addValue("Username", username);
		
		String sql = "SELECT ID FROM users WHERE Username=:Username";
		
		try {
			int id = Integer.valueOf(namedParamJdbcTemplate.queryForObject(sql, source,Integer.class));
			return id;
		} catch (Exception e) {
			return 0;
		}
	}
	
	@Override
	public boolean userExist(String username) {
		MapSqlParameterSource source = new MapSqlParameterSource();
		source.addValue("Username", username);
		
		String sql = "SELECT COUNT(Username) FROM users WHERE Username=:Username";
		
		try {
			int count = Integer.valueOf(namedParamJdbcTemplate.queryForObject(sql, source,Integer.class));
			return count > 0;
		} catch (Exception e) {
			return false;
		}
	}

	@Override
	public boolean accountExist(String username, String password) {
		MapSqlParameterSource source = new MapSqlParameterSource();
		source.addValue("Username", username);
		source.addValue("Password", password);
		
		String sql = "SELECT COUNT(ID) FROM users WHERE Username=:Username AND Password=MD5(:Password)";
		
		try {
			int count = Integer.valueOf(namedParamJdbcTemplate.queryForObject(sql, source,Integer.class));
			return count > 0;
		} catch (Exception e) {
			return false;
		}
	}
	
	@Override
	public boolean emailExist(String emailAddress) {
		MapSqlParameterSource source = new MapSqlParameterSource();
		source.addValue("EmailAddress", emailAddress);
		
		String sql = "SELECT COUNT(EmailAddress) FROM contacts WHERE EmailAddress=:EmailAddress";
		
		try {
			int count = Integer.valueOf(namedParamJdbcTemplate.queryForObject(sql, source,Integer.class));
			return count > 0;
		} catch (Exception e) {
			return false;
		}
	}

	@Override
	public boolean phoneExist(String phoneNumber) {
		MapSqlParameterSource source = new MapSqlParameterSource();
		source.addValue("PhoneNumber", phoneNumber);
		
		String sql = "SELECT COUNT(PhoneNumber) FROM contacts WHERE PhoneNumber=:PhoneNumber";
		
		try {
			int count = Integer.valueOf(namedParamJdbcTemplate.queryForObject(sql, source,Integer.class));
			return count > 0;
		} catch (Exception e) {
			return false;
		}
	}
	
	@Override
	public Account getAccountByUsername(String username) {
		SqlParameterSource param = new MapSqlParameterSource("Username",username);
		String sql = "SELECT * FROM `users` " + 
				"INNER JOIN names ON users.ID=names.UserID " + 
				"INNER JOIN addresses ON users.ID=addresses.UserID " + 
				"INNER JOIN contacts ON users.ID=contacts.UserID " + 
				"WHERE users.Username=:Username";
		try {
			Account account = namedParamJdbcTemplate.queryForObject(sql, param, new AccountRowMapper());
			return account;
		}catch(EmptyResultDataAccessException e) {
			return null;
		}
	}
	
	class AccountRowMapper implements RowMapper<Account>{

		@Override
		public Account mapRow(ResultSet rs, int rowNum) throws SQLException {
			Account account = new Account();
			account.setId(rs.getInt("ID"));
			account.setUsername(rs.getString("Username"));
			account.setPassword(rs.getString("Password"));
			account.setFirstName(rs.getString("FirstName"));
			account.setMiddleName(rs.getString("MiddleName"));
			account.setLastName(rs.getString("LastName"));
			account.setZipCode(rs.getString("ZipCode"));
			account.setStreetName(rs.getString("StreetName"));
			account.setTownName(rs.getString("TownName"));
			account.setProvinceName(rs.getString("ProvinceName"));
			account.setPhoneNumber(rs.getString("PhoneNumber"));
			account.setEmailAddress(rs.getString("EmailAddress"));
			return account;
		}
		
	}

}
