package com.ms3inc.backend.daoimplementation;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Component;

import com.ms3inc.backend.dao.GenericDao;
import com.ms3inc.backend.domain.Address;

@Component
public class AddressDaoImplementation implements GenericDao {

	private NamedParameterJdbcTemplate namedParamJdbcTemplate;
	
	@Autowired
	@Override
	public void setDataSource(DataSource ds) {
		namedParamJdbcTemplate = new NamedParameterJdbcTemplate(ds);
	}

	@Override
	public boolean insert(Object obj) {
		Address address = (Address) obj;
		SqlParameterSource source = new BeanPropertySqlParameterSource(address);
		
		String sql = "INSERT INTO addresses(ZipCode, StreetName, TownName, ProvinceName) VALUES(:zipCode, :streetName, :townName, :provinceName)";
		
		if (namedParamJdbcTemplate.update(sql, source) > 0) {
			return true;
		}
		
		return false;
	}

	@Override
	public boolean update(Object obj) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean delete(Object obj) {
		// TODO Auto-generated method stub
		return false;
	}

}
