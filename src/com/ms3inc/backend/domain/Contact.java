package com.ms3inc.backend.domain;

public class Contact {
	private int id;
	private String phoneNumber;
	private String emailAddress;

	public Contact() {
		
	}
	
	public Contact(String phoneNumber, String emailAddress) {
		super();
		this.phoneNumber = phoneNumber;
		this.emailAddress = emailAddress;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

}
