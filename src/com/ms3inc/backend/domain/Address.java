package com.ms3inc.backend.domain;

public class Address {

	private int id;
	private String zipCode;
	private String streetName;
	private String townName;
	private String provinceName;
	
	public Address() {
		
	}
	
	public Address(String zipCode, String streetName, String townName, String provinceName) {
		super();
		this.zipCode = zipCode;
		this.streetName = streetName;
		this.townName = townName;
		this.provinceName = provinceName;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getStreetName() {
		return streetName;
	}

	public void setStreetName(String streetName) {
		this.streetName = streetName;
	}

	public String getTownName() {
		return townName;
	}

	public void setTownName(String townName) {
		this.townName = townName;
	}

	public String getProvinceName() {
		return provinceName;
	}

	public void setProvinceName(String provinceName) {
		this.provinceName = provinceName;
	}

}
