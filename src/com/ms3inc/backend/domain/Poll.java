package com.ms3inc.backend.domain;

public class Poll {
	
	private int id;
	private int userid;
	private String code;
	private String title;
	
	public Poll() {
	
	}
	
	public Poll(int id, int userid) {
		super();
		this.id = id;
		this.userid = userid;
	}
	
	public Poll(int id, int userid, String title) {
		super();
		this.id = id;
		this.userid = userid;
		this.title = title;
	}
	
	public Poll(int userid, String code, String title) {
		super();
		this.userid = userid;
		this.code = code;
		this.title = title;
	}

	public Poll(String title) {
		super();
		this.title = title;
	}

	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getUserid() {
		return userid;
	}

	public void setUserid(int userid) {
		this.userid = userid;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

}
